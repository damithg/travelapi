﻿using System.Collections.Generic;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Deserializers;
using PingRequest = travelapi.Providers.Expedia.Search.PingRequest;

namespace travelapi.Providers.Expedia
{
    public partial class ExpediaClient
    {
        private T SearchMethod<T>(string method, string query, int page, string language = null, bool? includeAdult = null, int year = 0, string dateFormat = null) where T : new()
        {
            RestRequest req = new RestRequest("{method}");
            
            req.RequestFormat = DataFormat.Json;
            req.AddUrlSegment("method", method);
            //req.AddParameter("query", query);
            req.AddParameter("locale", "en_US");
            req.AddParameter("currencyCode", "USD");
            req.AddParameter("city", "Seattle");
            req.AddParameter("stateProvinceCode", "WA");
            req.AddParameter("arrivalDate", "04/09/2015");
            req.AddParameter("departureDate", "04/12/2015");
            req.AddParameter("room1", "2");

            if (language != null)
                req.AddParameter("language", language);

            if (page >= 1)
                req.AddParameter("page", page);
            if (year >= 1)
                req.AddParameter("year", year);
            if (includeAdult.HasValue)
                req.AddParameter("include_adult", includeAdult.Value ? "true" : "false");

            _client.AddHandler("application/json", new DotNetXmlDeserializer());

            IRestResponse<T> resp = _client.Get<T>(req);

            return resp.Data;
        }

        public SearchContainer<PingRequest> SearchMovie(string query, int page = 0, bool includeAdult = false, int year = 0)
        {
            return SearchMovie(query, DefaultLanguage, page, includeAdult, year);
        }

        public SearchContainer<PingRequest> SearchMovie(string query, string language, int page = 0, bool includeAdult = false, int year = 0)
        {
            return SearchMethod<SearchContainer<PingRequest>>("ping", query, page, language, includeAdult, year, "yyyy-MM-dd");
        }

        public SearchContainer<IList<HotelListResponse>> GetHotelList(string query, string language, int page = 0,
            bool includeAdult = false, int year = 0)
        {
            return SearchMethod<SearchContainer<IList<HotelListResponse>>>("list", query, page, language, includeAdult, year, "yyyy-MM-dd");
        }
    }


    public class DotNetXmlDeserializer : IDeserializer
    {
        public string DateFormat { get; set; }

        public string Namespace { get; set; }

        public string RootElement { get; set; }

        public HotelListResponse Deserialize<HotelListResponse>(IRestResponse response)
        {
            if (string.IsNullOrEmpty(response.Content))
            {
                return default(HotelListResponse);
            }

            var c = "{\"HotelListResponse\":{\"cacheKey\":\"4627d1c2:149e4611c7e:-7995\",\"cacheLocation\":\"10.186.170.113:7300\",\"customerSessionId\":\"0ABAAA71-7D1C-2914-9E42-611C7E90799A\",\"moreResultsAvailable\":true,\"numberOfRoomsRequested\":1}}";

            var a = JsonConvert.DeserializeObject<HotelListResponse>(c);
            return JsonConvert.DeserializeObject<HotelListResponse>(response.Content);

            //using (var stream = new MemoryStream(Encoding.UTF8.GetBytes(response.Content)))
            //{
            //    Friends facebookFriends = new JavaScriptSerializer().Deserialize<Friends>(result);

            //    var serializer = new DataContractJsonSerializer(typeof(T));
            //    return (T)serializer. Deserialize(stream);
            //}
        }
    }
}
