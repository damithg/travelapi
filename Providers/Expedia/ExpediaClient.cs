﻿using System;
using RestSharp;
using RestSharp.Deserializers;
using travelapi.Providers.Expedia.RestCall;

namespace travelapi.Providers.Expedia
{
    public partial class ExpediaClient
    {
        public string ApiKey { get; private set; }

        /// <summary>
        /// The session id that will be used when TMDb requires authentication
        /// </summary>
        /// <remarks>Use 'SetSessionInformation' to assign this value</remarks>
        public string SessionId { get; private set; }

        /// <summary>
        /// The type of the session id, this will determine the level of access that is granted on the API
        /// </summary>
        /// <remarks>Use 'SetSessionInformation' to assign this value</remarks>
        //public SessionType SessionType { get; private set; }

        /// <summary>
        /// The account details of the user account associated with the current user session
        /// </summary>
        /// <remarks>This value is automaticly populated when setting a user session</remarks>
        //public AccountDetails ActiveAccount { get; private set; }

        /// <summary>
        /// ISO 639-1 code. Ex en
        /// </summary>
        public string DefaultLanguage { get; set; }

        /// <summary>
        /// ISO 3166-1 code. Ex. US
        /// </summary>
        public string DefaultCountry { get; set; }

        /// <summary>
        /// The maximum number of times a call to TMDb will be retied
        /// </summary>
        /// <remarks>Default is 0</remarks>
        public int MaxRetryCount
        {
            get { return _client.MaxRetryCount; }
            set { _client.MaxRetryCount = value; }
        }

        /// <summary>
        /// The base number of seconds that will be waited between retry attempts.
        /// Each retry will take progressively longer to give the service a chance to recover from what ever the problem is.
        /// Formula: RetryAttempt * RetryWaitTimeInSeconds, this is the amount of time in seconds the application will wait before retrying
        /// </summary>
        /// <remarks>Default is 10</remarks>
        public int RetryWaitTimeInSeconds
        {
            get { return _client.RetryWaitTimeInSeconds; }
            set { _client.RetryWaitTimeInSeconds = value; }
        }

        public bool HasConfig { get; private set; }

        private const string ApiVersion = "3";
        private const string ProductionUrl = "dev.api.ean.com/ean-services/rs/hotel/v3";
        private ExpediaRestClient _client;
        
        public ExpediaClient(string apiKey, bool useSsl = false, string baseUrl = ProductionUrl)
        {
            DefaultLanguage = null;
            DefaultCountry = null;

            Initialize(baseUrl, useSsl, apiKey);
        }

        private void Initialize(string baseUrl, bool useSsl, string apiKey)
        {
            ApiKey = apiKey;

            string httpScheme = useSsl ? "https" : "http";
            _client = new ExpediaRestClient(String.Format("{0}://{1}/", httpScheme, baseUrl));
            _client.AddDefaultParameter("apiKey", apiKey, ParameterType.QueryString);
            _client.AddDefaultParameter("cid", "393411", ParameterType.QueryString);
            _client.AddDefaultParameter("minorRev", "99", ParameterType.QueryString);

            

            _client.ClearHandlers();
            _client.AddHandler("application/json", new JsonDeserializer());
        }

        
        /// <summary>
        /// Use this method to set the current client's authentication information.
        /// The session id assigned here will be used by the client when ever TMDb requires it.
        /// </summary>
        /// <param name="sessionId">The session id to use when making calls that require authentication</param>
        /// <param name="sessionType">The type of session id</param>
        /// <remarks>
        /// - Use the 'AuthenticationGetUserSession' and 'AuthenticationCreateGuestSession' methods to optain the respective session ids.
        /// - User sessions have access to far for methods than guest sessions, these can currently only be used to rate media.
        /// </remarks>
        
        /// <summary>
        /// Used internally to determine if the current client has the required session set, if not an appropriate exception will be thrown
        /// </summary>
        /// <param name="sessionType">The type of session that is required by the calling method</param>
        /// <exception cref="UserSessionRequiredException">Thrown if the calling method requires a user session and one isn't set on the client object</exception>
        /// <exception cref="GuestSessionRequiredException">Thrown if the calling method requires a guest session and no session is set on the client object. (neither user or client type session)</exception>
        
    }
}