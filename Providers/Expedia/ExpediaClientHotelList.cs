﻿using System;
using RestSharp;

namespace travelapi.Providers.Expedia
{
    public partial class ExpediaClient
    {
        public Lists.List GetList(string listId)
        {
            if (string.IsNullOrWhiteSpace(listId))
                throw new ArgumentNullException("listId");

            RestRequest request = new RestRequest("list/{listId}");
            request.AddUrlSegment("listId", listId);
            request.AddParameter("stateProvinceCode", "WA");
            request.AddParameter("countryCode", "US");
            request.AddParameter("arrivalDate", "8/14/2014");
            request.AddParameter("departureDate", "8/16/2014");
            request.AddParameter("city", "Seattle");
            request.DateFormat = "dd/MM/yyyy";

            IRestResponse<Lists.List> response = _client.Get<Lists.List>(request);

            return response.Data;
        }
    }
}
