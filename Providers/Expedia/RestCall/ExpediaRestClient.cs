﻿using System;
using System.Net;
using System.Threading;
using RestSharp;

namespace travelapi.Providers.Expedia.RestCall
{
    internal class ExpediaRestClient : RestClient
    {
        public int MaxRetryCount { get; set; }
		public int RetryWaitTimeInSeconds { get; set; }

		public ExpediaRestClient()
			: base()
		{
			InitializeDefaults();
		}

        public ExpediaRestClient(string baseUrl)
			: base(baseUrl)
		{
			InitializeDefaults();
		}

		private void InitializeDefaults()
		{
			MaxRetryCount = 0;
			RetryWaitTimeInSeconds = 10;
		}

        public override IRestResponse<T> Execute<T>(IRestRequest request)
        {
            IRestResponse<T> response = base.Execute<T>(request);

            if (response.ErrorException != null)
            {
                if (MaxRetryCount >= request.Attempts && response.ErrorException.GetType() == typeof(WebException))
                {
                    WebException webException = (WebException)response.ErrorException;
                    // Retry the call after waiting the configured ammount of time, it gets progressively longer every retry
                    Thread.Sleep(request.Attempts * RetryWaitTimeInSeconds * 1000);
                    return this.Execute<T>(request);
                }
                else
                {
                    throw response.ErrorException;
                }
            }

            if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                throw new UnauthorizedAccessException("Call to TMDb returned unauthorized. Most likely the provided API key is invalid.");
            }

            return response;
        }
    }
}
