﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace travelapi.Providers.Expedia
{
    public class HotelListResponse
    {
        //public List<HotelList> HotelList { get; set; }
        [JsonProperty("cacheKey")]
        public string cacheKey { get; set; }
        [JsonProperty("cacheLocation")]
        public string cacheLocation { get; set; }

        //public CachedSupplierResponse cachedSupplierResponse { get; set; }
        [JsonProperty("customerSessionId")]
        public string customerSessionId { get; set; }
        [JsonProperty("moreResultsAvailable")]
        public bool moreResultsAvailable { get; set; }
        [JsonProperty("numberOfRoomsRequested")]
        public int numberOfRoomsRequested { get; set; }
    }

    public class HotelList
    {
        //public string ActivePropertyCount { get; set; }
        //public string Size { get; set; }
        public List<HotelSummary> HotelSummary { get; set; }
    }


    public class HotelSummary
    {
        //[JsonProperty(PropertyName = "@order")]
        //public string order {get;set;}
        //public string ubsScore {get;set;}
        //public RoomRateDetailsList RoomRateDetailsList { get; set; }
        public string address1 { get; set; }
        //public string airportCode { get; set; }
        //public int amenityMask { get; set; }
        //public string city { get; set; }
        //public int confidenceRating { get; set; }
        //public string countryCode { get; set; }
        //public string deepLink { get; set; }
        //public double highRate { get; set; }
        //public int hotelId { get; set; }
        //public bool hotelInDestination { get; set; }
        //public double hotelRating { get; set; }
        //public double latitude { get; set; }
        //public string locationDescription { get; set; }
        //public double longitude { get; set; }
        //public double lowRate { get; set; }
        //public string name { get; set; }
        //public int postalCode { get; set; }
        //public int propertyCategory { get; set; }
        //public double proximityDistance { get; set; }
        //public string proximityUnit { get; set; }
        //public string rateCurrencyCode { get; set; }
        //public string shortDescription { get; set; }
        //public string stateProvinceCode { get; set; }
        //public string supplierType { get; set; }
        //public string thumbNailUrl { get; set; }
        //public double tripAdvisorRating { get; set; }
        //public string tripAdvisorRatingUrl { get; set; }
        //public int tripAdvisorReviewCount { get; set; }
    }

    public class RoomRateDetails
    {
        public RateInfos RateInfos { get; set; }
        public int expediaPropertyId { get; set; }
        public int maxRoomOccupancy { get; set; }
        public int minGuestAge { get; set; }
        public bool propertyAvailable { get; set; }
        public bool propertyRestricted { get; set; }
        public int quotedRoomOccupancy { get; set; }
        public int rateCode { get; set; }
        public string roomDescription { get; set; }
        public int roomTypeCode { get; set; }
        public ValueAdds ValueAdds { get; set; }
    }

    public class RoomRateDetailsList
    {
        public RoomRateDetails RoomRateDetails { get; set; }
    }


    public class Room
{
    public int numberOfAdults { get; set; }
    public int numberOfChildren { get; set; }
}

    public class RoomGroup
    {
        public Room Room { get; set; }
    }

    public class HotelFee
{
    public string amount { get; set; }
    public string description { get; set; }
}

public class HotelFees
{
    public string size { get; set; }
    public HotelFee HotelFee { get; set; }
}

public class RateInfo
{
    public string priceBreakdown { get; set; }
    public string promo { get; set; }
    public string rateChange { get; set; }
    public ChargeableRateInfo ChargeableRateInfo { get; set; }
    public RoomGroup RoomGroup { get; set; }
    public int currentAllotment { get; set; }
    public bool nonRefundable { get; set; }
    public string rateType { get; set; }
    public HotelFees HotelFees { get; set; }
    public string promoDescription { get; set; }
    public int? promoId { get; set; }
    public string promoType { get; set; }
}

public class RateInfos
{
    public string size { get; set; }
    public RateInfo RateInfo { get; set; }
}

public class ValueAdds
{
    public string size { get; set; }
    public object ValueAdd { get; set; }
}

    public class NightlyRate
{
    public string baseRate { get; set; }
    public string promo { get; set; }
    public string rate { get; set; }
}

public class NightlyRatesPerRoom
{
    public string size { get; set; }
    public List<NightlyRate> NightlyRate { get; set; }
}

public class Surcharge
{
    public string amount { get; set; }
    public string type { get; set; }
}

public class Surcharges
{
    public string size { get; set; }
    public Surcharge Surcharge { get; set; }
}

public class ChargeableRateInfo
{
    public string averageBaseRate { get; set; }
    public string averageRate { get; set; }
    public string commissionableUsdTotal { get; set; }
    public string currencyCode { get; set; }
    public string maxNightlyRate { get; set; }
    public string nightlyRateTotal { get; set; }
    public string surchargeTotal { get; set; }
    public string total { get; set; }
    public NightlyRatesPerRoom NightlyRatesPerRoom { get; set; }
    public Surcharges Surcharges { get; set; }
}
}
