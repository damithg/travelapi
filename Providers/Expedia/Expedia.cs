﻿using System.Security.Cryptography;
using System.ServiceModel;
using System.Text;

namespace travelapi.Providers.Expedia
{
    public class Expedia
    {
        public void GetHotelList()
        {
            ExpediaClient client = new ExpediaClient("rgzua9kv2sutzkeycwwqbj9f");
            //ExpediaClient client = new ExpediaClient("cbrzfta369qwyrm9t5b8y8kf");  

            string query = "list";
            var result = client.GetHotelList(query, "<HotelListRequest> <city>Seattle</city> <stateProvinceCode>WA</stateProvinceCode> <countryCode>US</countryCode> <arrivalDate>12/23/2014</arrivalDate> <departureDate>12/25/2014</departureDate> <RoomGroup> <Room> <numberOfAdults>2</numberOfAdults> </Room> </RoomGroup> <numberOfResults>25</numberOfResults> </HotelListRequest>");

        }

        public void GetPingRequest()
        {
            ExpediaClient client = new ExpediaClient("bahdrpsc9y3ndn58ucmnjyk8");

            string query = "ping";
            var result = client.SearchMovie(query);

        }

        public void GetHotels()
        {
            //HotelRoomReservationRequest request = new HotelRoomReservationRequest();
            //request.arrivalDate = "07/07/2012";
            //request.departureDate = "07/09/2012";
            //request.cid = 55505;// 393411;
            //request.currencyCode = "GBR";
            //request.apiKey = "bahdrpsc9y3ndn58ucmnjyk8";
            ////request.
            //HotelListRequest list = new HotelListRequest();
            //list.city = "Paris";
            //list.currencyCode = "EUR";
            //list.supplierType = "E";
            //list.customerIpAddress = "81.111.46.151";
            //Room room = new Room();
            //room.numberOfAdults = 1;

            //list.RoomGroup = new Room[] { room };
            //list.locale = LocaleType.en_US;
            //list.stateProvinceCode = "TX";
            //list.countryCode = "US";
            //list.numberOfResults = 10;
            //list.cid = 55505;//393411;
            //list.arrivalDate = "07/07/2012";
            //list.departureDate = "08/07/2012";
            //list.numberOfBedRooms = 1;
            //list.apiKey = "bahdrpsc9y3ndn58ucmnjyk8";
            //list.sig = MD5GenerateHash(list.apiKey + "9zK3mgqq" + (Int32)(DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds);

            //list.customerIpAddress = "81.111.46.151";

            //PingRequest re = new PingRequest();
            //re.apiKey = "bahdrpsc9y3ndn58ucmnjyk8";
            //re.customerUserAgent = "Mozilla/5.0+(Windows+NT+6.1;+WOW64)+AppleWebKit/536.5+(KHTML,+like+Gecko)+Chrome/19.0.1084.56+Safari/536.5";
            //re.customerIpAddress = "81.111.46.151";
            ////re.cid = 393411;
            //re.cid = 55505;
            //re.echo = "test";
            //re.minorRev = 99;


            //var ab = CreateServiceClient<string>("http://dev.api.ean.com/ean-services/rs/hotel/v3");

            //HotelServicesClient hotelServicesClient = new HotelServicesClient("HotelServicesImplPort");

            //var a = hotelServicesClient.getPing(re);


            //ExpediaHotel.getPing getPing = new getPing();
            //getPing.PingRequest = re;

            //PingResponse response = hotelServicesClient.getPing(re);
            
            ////HotelRoomReservationResponse reservationResponse = 
            //HotelListResponse listResponse = hotelServicesClient.getList(list);
           
            //HotelServicesClient hotelServicesClient = new HotelServicesClient("HotelServicesImplPort");
        }

        private static string MD5GenerateHash(string strInput)
        {
            // Create a new instance of the MD5CryptoServiceProvider object.
            MD5 md5Hasher = MD5.Create();
  
            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(strInput));

            // Create a new Stringbuilder to collect the bytes and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data and format each one as a hexadecimal string.
            for (int nIndex = 0; nIndex < data.Length; ++nIndex)
            {
                sBuilder.Append(data[nIndex].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }


        private static T CreateServiceClient<T>(string devEndpointAddress)
        {
            var binding = new BasicHttpBinding();

            // WARNING: Need to ensure we are looking at DEV/UAT service
            var devEndpoint = new EndpointAddress(devEndpointAddress);

            var myChannelFactory = new ChannelFactory<T>(binding, devEndpoint);

            return myChannelFactory.CreateChannel();
        }


        public void GetHotel()
        {
            //HotelServicesClient client = new HotelServicesClient("HotelServicesImplPort");
            //HotelListRequest hotelListRequest = new HotelListRequest();
            //HotelList hotelList = new HotelList();
            //HotelListResponse hotelListResponse = new HotelListResponse();
            //client.Open();
            //hotelListRequest.apiKey = "bahdrpsc9y3ndn58ucmnjyk8";//use ur own key
            //hotelListRequest.cid = 55505; //this is the CID for testing
            //hotelListRequest.city = "Riyadh";
            //hotelListRequest.datelessSupplierSort = true;
            //hotelListResponse = client.getList(hotelListRequest);
            //hotelList = hotelListResponse.HotelList;
            //for (int i = 0; i < hotelList.size; i++)
            //{
            //    Debug.Write(hotelList.HotelSummary[i].name);
            //    Debug.Write("</br>");
            //}
        }
    }
}
