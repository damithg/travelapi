﻿using NUnit.Framework;
using travelapi.Providers.Expedia;

namespace travelapi.Integration
{
    public class Class1
    {
        [Test]
        public void TestMethod1()
        {
            Expedia expedia = new Expedia();
            //expedia.GetHotels();
            expedia.GetHotel();

        }

        [Test]
        public void DirectCall()
        {
            Expedia expedia = new Expedia();
            expedia.GetPingRequest();
        }

        [Test]
        public void GetHotelList()
        {
            Expedia expedia = new Expedia();
            expedia.GetHotelList();
        }
    }
}
